<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'rVTH/QsywhJkDtUVb2G95aKr+7k6ZNDvtyHOPRRzG+29D+M0jsXHCSke4rU8n9TIYzdxX+LhML94Gfv4SS8c3w==');
define('SECURE_AUTH_KEY',  '4ttoke/XZT+COLURk+tE1MSGn02tKNk/JQIJSlrQSloKoT23SmYA8HxOrlDxRMOv2qfgkb+aiyAcxZeSc2A9Jw==');
define('LOGGED_IN_KEY',    'vZukqFrQPKLL7EpBWe7MujFLeF+syLdb+r6fLNe9GPVYKmmGOxzVSgS83oDVn9TWMEzSg6lcuSgDs1ZAq4MlYw==');
define('NONCE_KEY',        'pn94zJZBX0d+azOOW1NE90kLqtNDwJDU8WcjR2zLy2muvUdMgmBveWWqfZSQEbGGtK5MIueombpMWnvX+jMeTA==');
define('AUTH_SALT',        'R0tCGQVfUkqJw0DCdjsIUbL8PyNtjmZ2BRHiDcHBXbMtGKveP2y7AppZLjttifav53zg1Yhg/w0JQdNPv3SOcQ==');
define('SECURE_AUTH_SALT', '1mUxzW7KxmH+K1ntxleA0+6+JvtIAngKndGqcZw0xDGC3eamhPc/2E+AlDmo4cwaDADde1bAgZkb5EUFExitfw==');
define('LOGGED_IN_SALT',   '3D+5xgcA7bKeYwd9mscfmmX/uMNK4oGMdjOvEiAc7fwHLd3YSIxiJKclS6wgQJRoOJ+r+TvK15m8ZPA3SsYRWQ==');
define('NONCE_SALT',       'HwwKMJXCjltQulamMCW/wXRNfi/pWPLf0wGP2GNKPCk3gzlUgXCgmRSB/NIpEoW/a0Jk19tITTw+5tZRmK7USA==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
