<?php

    Kirki::add_panel( 'cl_custom_types', array(
	    'priority'    => 20,
	    'type' => '',
	    'title'       => esc_html__( 'Custom Types', 'june' ),
	    'tooltip' => esc_html__( 'All Custom Types Options', 'june' ),
	) );
	    
	    
	    Kirki::add_section( 'cl_custom_portfolio', array(
    	    'title'          => esc_html__( 'Portfolio', 'june' ),
    	    'tooltip'    => esc_html__( 'All Portfolio Page and single options', 'june' ),
    	    'panel'          => 'cl_custom_types',
    	    'type'			 => '',
    	    'priority'       => 160,
    	    'capability'     => 'edit_theme_options'
    	) );

    	Kirki::add_section( 'cl_custom_staff', array(
    	    'title'          => esc_html__( 'Staff', 'june' ),
    	    'tooltip'    => esc_html__( 'All Staff (Members) options', 'june' ),
    	    'panel'          => 'cl_custom_types',
    	    'type'			 => '',
    	    'priority'       => 160,
    	    'capability'     => 'edit_theme_options'
    	) );
 
    	Kirki::add_section( 'cl_custom_testimonial', array(
    	    'title'          => esc_html__( 'Testimonial', 'june' ),
    	    'tooltip'    => esc_html__( 'All Testimonial options', 'june' ),
    	    'panel'          => 'cl_custom_types',
    	    'type'			 => '',
    	    'priority'       => 160,
    	    'capability'     => 'edit_theme_options'
    	) );
    	

    	Kirki::add_field( 'cl_june', array(

			'settings' => 'portfolio_slug',
			'label'    => esc_html__( 'Portfolio Slug', 'june' ),
			'tooltip' => esc_html__( 'Used as prefix for portfolio items links', 'june' ),
			'section'  => 'cl_custom_portfolio',
			'type'     => 'text',
			'default'  => 'portfolio_items',
			'transport' => 'postMessage'

		) );

    	Kirki::add_field( 'cl_june', array(
			'settings' => 'portfolio_main_page',
			'label'    => esc_html__( 'Portfolio Main Page', 'june' ),
			'tooltip' => esc_html__( 'Set portfolio main page, useful in portfolio single navigation.', 'june' ),
			'section'  => 'cl_custom_portfolio',
			'type'     => 'select',
			'priority' => 10,
			'default'  => 'none',
			'transport' => 'postMessage',
			'choices'  =>   codeless_get_pages()
		) );

			
			

?>