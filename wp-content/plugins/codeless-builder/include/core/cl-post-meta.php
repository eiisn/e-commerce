<?php


class Cl_Post_Meta{
    
    
    public static $meta = array();
    public $post_meta = array();

    public function __construct(){
        
        
    }
    
    public function init(){
        
        add_action( 'init', array(&$this, 'post_type_support'), 99);
        add_action( 'customize_posts_register_meta', array( &$this, 'register_post_meta') );
        //add_filter( 'customize_posts_partial_schema', array( $this, 'filter_partial_schema' ), 18 );
        add_action( 'customize_controls_enqueue_scripts', array( &$this, 'enqueue_meta_scripts' ), 10 );
        add_action( 'customize_preview_init', array( &$this, 'enqueue_postMessage' ), 10 );
        add_action( 'customize_controls_init', array(&$this, 'remove_tinymce'), 0 );
        add_filter( 'customize_posts_partial_schema', array( $this, 'filter_partial_schema' ) );
        $this->remove_customize_posts();
    }

    public function filter_partial_schema($schema){
        
        $schema['post_title']['fallback_refresh'] = false;

        return $schema;
    }

    public function remove_customize_posts(){ 
        if( isset($_GET['mode']) && $_GET['mode'] == 'simple' ){
            global $customize_posts_plugin;
            remove_action( 'wp_default_scripts', array( $customize_posts_plugin, 'register_scripts' ), 11 );
            remove_action( 'wp_default_styles', array( $customize_posts_plugin, 'register_styles' ), 11 );
            remove_action( 'init', array( $customize_posts_plugin, 'register_customize_draft' ) );
            remove_action( 'admin_bar_menu', array( $customize_posts_plugin, 'add_admin_bar_customize_link_queried_object_autofocus' ), 41 );
            remove_action( 'user_has_cap', array( $customize_posts_plugin, 'grant_customize_capability' ), 10, 3 );
            remove_action( 'customize_loaded_components', array( $customize_posts_plugin, 'add_posts_to_customize_loaded_components' ), 0, 1 );
            remove_action( 'customize_loaded_components', array( $customize_posts_plugin, 'filter_customize_loaded_components' ), 100, 2 );
            remove_action( 'customize_register', array( $customize_posts_plugin, 'load_support_classes' ) );
        }
    }

    public function remove_tinymce(){
        global $wp_customize;
        if( isset($_GET['mode']) && $_GET['mode'] == 'simple' ){
        // @todo These should be included in _WP_Editors::editor_settings()
            remove_action( 'customize_controls_print_footer_scripts', array( '_WP_Editors', 'enqueue_scripts' ) );
        }
    }


    /*ublic function filter_customize_dynamic_partial_args( $args, $id ){
        if ( preg_match( WP_Customize_Post_Field_Partial::ID_PATTERN, $id, $matches ) ) {
            $post_type_obj = get_post_type_object( $matches['post_type'] );

            $field_id = $matches['field_id'];
            if ( ! empty( $matches['placement'] ) ) {
                $field_id .= '[' . $matches['placement'] . ']';
            }
            $schema = $this->get_post_field_partial_schema( $field_id );
            if ( ! empty( $schema ) ) {
                $args = array_merge( $args, $schema );
            }
        }
        return $args;
    }*/


    public function load_post_meta(){
        if( is_file( cl_path_dir( 'THEME_CODELESS_CONFIG', 'cl-post-meta.php' ) ) )
            require_once cl_path_dir( 'THEME_CODELESS_CONFIG', 'cl-post-meta.php' );
        else
            require_once cl_path_dir( 'CONFIG_DIR', 'cl-post-meta.php' );
    }
    
    public function post_type_support(){
        //$this->load_post_meta();
        if(empty(self::$meta))
            return;
        
        
        foreach(self::$meta as $post_meta){
            if(is_array($post_meta['post_type'])){
                foreach($post_meta['post_type'] as $post_type){
                    add_post_type_support( $post_type, $post_meta['feature'] );
                }
            }else{
                
                add_post_type_support( $post_meta['post_type'], $post_meta['feature'] );
            }
            
        }

    }
    
    public function sanitize_value( $meta_value ) {
		return $meta_value;
	}
    
    
    public function register_post_meta( \WP_Customize_Posts $customize_posts ){
        
        if(empty(self::$meta))
            return;
            
        foreach(self::$meta as $post_meta){
    	    foreach ( get_post_types_by_support( $post_meta['feature'] ) as $post_type ) {
    		    $customize_posts->register_post_type_meta( $post_type, $post_meta['meta_key'], array(
    			    'transport' => $post_meta['transport'],
    			    'post_type_supports' => $post_meta['feature'],
    			    'sanitize_callback' => function( $value ) {
        				return $value;
        			},
    		    ) );
    	    }
        }
        
	}
	
	public function enqueue_meta_scripts(){
        
	    if( isset($_GET['mode']) && $_GET['mode'] == 'simple' )
            return;

	    wp_enqueue_script( 'cl-meta-controls', cl_asset_url('js/cl-meta-controls.js'), array('kirki-script') );
        wp_localize_script(
            'cl-meta-controls',
            'cl_post_meta',
            array(
                'metadata' => self::$meta,
            )
        );
	}


    public function enqueue_postMessage(){

        wp_enqueue_script( 'cl-meta-postmessage', cl_asset_url('js/cl-meta-postmessage.js') );
        wp_localize_script(
            'cl-meta-postmessage',
            'cl_post_meta',
            array(
                'metadata' => array_reverse(self::$meta),
            )
        );
    }

    
    
    public static function map($meta){
        if( ! function_exists( 'codeless_get_meta' ) )
            return;

        if( ! codeless_get_meta( $meta['meta_key'] ) )
            $meta['value'] = isset( $meta['default'] ) ? $meta['default'] : '';
        else
            $meta['value'] = codeless_get_meta( $meta['meta_key'] );

        self::$meta[] = $meta;
    }

    
}

?>