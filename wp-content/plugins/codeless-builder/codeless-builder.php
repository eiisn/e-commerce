<?php
/**
 * Plugin Name: Codeless Builder
 * Plugin URI: http://codeless.co
 * Description: Codeless Page, Header, Footer Builder in WP Customizer
 * Version: 1.4.6
 * Author: Codeless
 * Author URI: http://codeless.co
 * License: GPL2
 */
 
 // don't load directly
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}
/**
 * Current version
 */
if ( ! defined( 'CL_BUILDER_VERSION' ) ) {

	define( 'CL_BUILDER_VERSION', '1.4.6' );
}




class Cl_Builder_Manager{
    
    private $paths;
    
    private $factory = array();

	private $plugin_name;
	
	private $custom_user_templates_dir = false;
    
    private static $_instance;
    
    public function __construct(){
        
        $dir = dirname( __FILE__ );

        
		
		/**
		 * Define path settings for visual composer.
		 *
		 * APP_ROOT        - plugin directory.
		 * WP_ROOT         - WP application root directory.
		 * APP_DIR         - plugin directory name.
		 * CONFIG_DIR      - configuration directory.
		 * ASSETS_DIR      - asset directory full path.
		 * ASSETS_DIR_NAME - directory name for assets. Used from urls creating.
		 * CORE_DIR        - classes directory for core vc files.
		 * HELPERS_DIR     - directory with helpers functions files.
		 * SHORTCODES_DIR  - shortcodes classes.
		 * SETTINGS_DIR    - main dashboard settings classes.
		 * TEMPLATES_DIR   - directory where all html templates are hold.
		 * EDITORS_DIR     - editors for the post contents
		 * PARAMS_DIR      - complex params for shortcodes editor form.
		 * UPDATERS_DIR    - automatic notifications and updating classes.
		 */
		$this->setPaths( array(
			'APP_ROOT' => $dir,
			'WP_ROOT' => preg_replace( '/$\//', '', ABSPATH ),
			'APP_DIR' => basename( $dir ),
			'THEME_CODELESS_DIR' => get_template_directory().'/includes/codeless_builder',
			'THEME_CODELESS_CONFIG' => get_template_directory().'/includes/codeless_builder/config',
			'THEME_CODELESS_HEADER' => get_template_directory().'/includes/codeless_builder/header-elements',
			'THEME_CODELESS_SHORTCODES' => get_template_directory().'/includes/codeless_builder/shortcodes',
			'CONFIG_DIR' => $dir . '/config',
			'ASSETS_DIR' => $dir . '/assets',
			'ASSETS_DIR_NAME' => 'assets',
			'CORE_DIR' => $dir . '/include/core',
			'HELPERS_DIR' => $dir . '/include/helpers',
			'TEMPLATES_DIR' => $dir . '/include/templates',
			'SHORTCODES_DIR' => $dir . '/include/core/shortcodes'

		) );
		// Load API
		require_once $this->path( 'HELPERS_DIR', 'helpers.php' );
		require_once $this->path( 'CORE_DIR', 'cl-builder-mapper.php' );
		require_once $this->path( 'CORE_DIR', 'cl-register-post-type.php' );
		require_once $this->path( 'SHORTCODES_DIR', 'cl-shortcode.php' );
		require_once $this->path( 'SHORTCODES_DIR', 'cl-shortcode-manager.php' );
		require_once $this->path( 'SHORTCODES_DIR', 'cl-shortcode-container.php' );
		require_once $this->path( 'SHORTCODES_DIR', 'cl-shortcode-simple.php' );
		
		// Add hooks
		
		add_action( 'plugins_loaded', array( &$this, 'pluginsLoaded' ), 9 );
		
		add_action( 'init', array( &$this, 'init' ), 999 );

		remove_action('widgets_init', 'NEXForms_widget::register_this_widget');
		// Remove Widgets and Nav Menus on simple MODE
		//add_filter( 'customize_loaded_components', array( &$this,'remove_widgets_panel' ), 999, 1 );
		//add_filter( 'customize_loaded_components', array( &$this, 'remove_nav_menus_panel' ), 999, 1 );
		$this->setPluginName( $this->path( 'APP_DIR', 'codeless-builder.php' ) );
		//register_activation_hook( __FILE__, array( $this, 'activationHook' ) );
    }
    
    
    public static function getInstance() {
		if ( ! ( self::$_instance instanceof self ) ) {
			self::$_instance = new self();
		}

		return self::$_instance;
	}
	
    
    public function init() {

    	if( ! function_exists( 'codeless_setup' ) )
    		return;
    	
		/**
		 * Set version if required.
		 */
		$this->setVersion();


		/**
		 * Init default functions of plugin
		 */
		$this->cl_base()->init();
		
		/**
		 * Init Header Builder functionality
		 */
		$this->header_builder()->init();
		
		
		/**
		 * Init Page Builder functionality
		 */
		$this->page_builder()->init();
		
		
		//var_dump(Cl_Builder_Mapper::getShortcodes());
		
		
	}
	
    
    protected function setPaths( $paths ) {
		$this->paths = $paths;
	}
	
	
	public function path( $name, $file = '' ) {
		$path = $this->paths[ $name ] . ( strlen( $file ) > 0 ? '/' . preg_replace( '/^\//', '', $file ) : '' );
		return $path;
	}
	
	
	public function pluginsLoaded() {


		// Setup locale
		load_plugin_textdomain( 'cl_builder', false, $this->path( 'APP_DIR', 'locale' ) );
		
		/**
		 * Init Post Meta functionality
		 */
		if( cl_is_customize_posts_active() )
			$this->post_meta()->init();

		
	}
	
	
	protected function setVersion() {
		$version = get_option( 'cl_builder_version' );
		if ( ! is_string( $version ) || version_compare( $version, CL_BUILDER_VERSION ) !== 0 ) {
			update_option( 'cl_builder_version', CL_BUILDER_VERSION );
		}
	}
	
	public function setPluginName( $name ) {
		$this->plugin_name = $name;
	}
	
	
	public function cl_base() {
		if ( ! isset( $this->factory['cl_base'] ) ) {
		
			require_once $this->path( 'CORE_DIR', 'cl-builder-base.php' );
			$cl_base = new Cl_Builder_Base();
			
			$this->factory['cl_base'] = $cl_base;
		}

		return $this->factory['cl_base'];
	}
	
	
	public function header_builder() {
		if ( ! isset( $this->factory['cl_header_builder'] ) ) {
		
			require_once $this->path( 'CORE_DIR', 'cl-header-builder.php' );
			$cl_header_builder = new Cl_Header_Builder();
			
			$this->factory['cl_header_builder'] = $cl_header_builder;
		}

		return $this->factory['cl_header_builder'];
	}
	
	
	
	public function page_builder() {
		if ( ! isset( $this->factory['cl_page_builder'] ) ) {
		
			require_once $this->path( 'CORE_DIR', 'cl-page-builder.php' );
			$cl_page_builder = new Cl_Page_Builder();
			
			$this->factory['cl_page_builder'] = $cl_page_builder;
		}

		return $this->factory['cl_page_builder'];
	}
	
	public function post_meta() {
		if ( ! isset( $this->factory['cl_post_meta'] ) ) {
		
			require_once $this->path( 'CORE_DIR', 'cl-post-meta.php' );
			$cl_post_meta = new Cl_Post_Meta();
			
			$this->factory['cl_post_meta'] = $cl_post_meta;
		}

		return $this->factory['cl_post_meta'];
	}
	
	public function pluginUrl( $file ) {
		return preg_replace( '/\s/', '%20', plugins_url( $file , __FILE__ ) );
	}
	
	public function assetUrl( $file ) {
		return preg_replace( '/\s/', '%20', plugins_url( $this->path( 'ASSETS_DIR_NAME', $file ), __FILE__ ) );
	}
	
	public function getShortcodesTemplateDir( $template ) {
		return false !== $this->custom_user_templates_dir ? $this->custom_user_templates_dir . '/' . $template : locate_template( 'includes/codeless_builder/shortcodes' . '/' . $template );
	}
	
	public function getDefaultShortcodesTemplatesDir() {
		return cl_path_dir( 'TEMPLATES_DIR', 'shortcodes' );
	}

	public function getDefaultHeaderTemplatesDir() {
		return cl_path_dir( 'TEMPLATES_DIR', 'header-elements' );
	}

	public function remove_widgets_panel($components){

	}

	public function remove_nav_menus_panel($components){
		$components = array();
	    return $components;
	}
	
    
}


global $cl_builder;
if ( ! $cl_builder ) {
	$cl_builder = Cl_Builder_Manager::getInstance();
}


 
 
 ?>