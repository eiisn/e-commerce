<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

if ( ! class_exists( 'WPBakeryVisualComposerCssEditor' ) ) {
	/**
	 * Class WPBakeryVisualComposerCssEditor
	 * modified by codeless
	 */
	class WPBakeryVisualComposerCssEditor {
		/**
		 * @var array
		 */
		protected $settings = array();
		/**
		 * @var string
		 */
		protected $value = '';
		/**
		 * @var array
		 */
		protected $layers = array( 'margin', 'border', 'padding', 'content' );
		/**
		 * @var array
		 */
		protected $positions = array( 'top', 'right', 'bottom', 'left' );

		/**
		 *
		 */
		function __construct() {
		}

		/**
		 * Setters/Getters {{
		 *
		 * @param null $settings
		 *
		 * @return array
		 */
		function settings( $settings = null ) {
			if ( is_array( $settings ) ) {
				$this->settings = $settings;
			}

			return $this->settings;
		}

		/**
		 * @param $key
		 *
		 * @return string
		 */
		function setting( $key ) {
			return isset( $this->settings[ $key ] ) ? $this->settings[ $key ] : '';
		}

		/**
		 * @param null $value
		 *
		 * @return string
		 */
		function value( $value = null ) {
			if ( is_array( $value ) ) {
				$this->value = $value;
			}

			return $this->value;
		}

		/**
		 * @param null $values
		 *
		 * @return array
		 */
		function params( $values = null ) {
			if ( is_array( $values ) ) {
				$this->params = $values;
			}

			return $this->params;
		}

		// }}
		/**
		 * vc_filter: vc_css_editor - hook to override output of this method
		 * @return mixed|void
		 */
		function render() {
			$output = '<div class="vc_css-editor vc_row vc_ui-flex-row" data-css-editor="true">';
			$output .= $this->onionLayout();
			$output .= '</div>';

			return apply_filters( 'vc_css_editor', $output );
		}

		/**
		 * @return string
		 */
		function getBackgroundImageControl() {
			return apply_filters( 'vc_css_editor_background_image_control', '<ul class="vc_image">'
				. '</ul>'
			. '<a href="#" class="vc_add-image"><i class="vc-composer-icon vc-c-icon-add"></i>' . __( 'Add image', 'js_composer' ) . '</a>' );
		}

		/**
		 * @return string
		 */
		function getBorderRadiusOptions() {
			$radiuses = apply_filters( 'vc_css_editor_border_radius_options_data', array(
				'' => __( 'None', 'js_composer' ),
				'1px' => '1px',
				'2px' => '2px',
				'3px' => '3px',
				'4px' => '4px',
				'5px' => '5px',
				'10px' => '10px',
				'15px' => '15px',
				'20px' => '20px',
				'25px' => '25px',
				'30px' => '30px',
				'35px' => '35px',
			) );

			$output = '';
			foreach ( $radiuses as $radius => $title ) {
				$output .= '<option value="' . $radius . '">' . $title . '</option>';
			}

			return $output;
		}

		/**
		 * @return string
		 */
		function getBorderStyleOptions() {
			$output = '<option value="">' . __( 'Theme defaults', 'js_composer' ) . '</option>';
			$styles = apply_filters( 'vc_css_editor_border_style_options_data', array(
				'solid',
				'dotted',
				'dashed',
				'none',
				'hidden',
				'double',
				'groove',
				'ridge',
				'inset',
				'outset',
				'initial',
				'inherit',
			) );
			foreach ( $styles as $style ) {
				$output .= '<option value="' . $style . '">' . __( ucfirst( $style ), 'js_composer' ) . '</option>';
			}

			return $output;
		}

		/**
		 * @return string
		 */
		function getBackgroundStyleOptions() {
			$output = '<option value="">' . __( 'Theme defaults', 'js_composer' ) . '</option>';
			$styles = apply_filters( 'vc_css_editor_background_style_options_data', array(
				__( 'Cover', 'js_composer' ) => 'cover',
				__( 'Contain', 'js_composer' ) => 'contain',
				__( 'No Repeat', 'js_composer' ) => 'no-repeat',
				__( 'Repeat', 'js_composer' ) => 'repeat',
			) );
			foreach ( $styles as $name => $style ) {
				$output .= '<option value="' . $style . '">' . $name . '</option>';
			}

			return $output;
		}

		/**
		 * @return string
		 */
		function onionLayout() {
			$output = '<div class="vc_layout-onion vc_col-xs-7">'
			          . '    <div class="vc_margin">' . $this->layerControls( 'margin' )
			          . '      <div class="vc_border">' . $this->layerControls( 'border', 'width' )
			          . '          <div class="vc_padding">' . $this->layerControls( 'padding' )
			          . '              <div class="vc_content"><i></i></div>'
			          . '          </div>'
			          . '      </div>'
			          . '    </div>'
			          . '</div>';

			return apply_filters( 'vc_css_editor_onion_layout', $output );
		}

		/**
		 * @param $name
		 * @param string $prefix
		 *
		 * @return string
		 */
		protected function layerControls( $name, $prefix = '' ) {
			
			$output = '<label>' . $name . '</label>';
			foreach ( $this->positions as $pos ) {

				if( $name == 'border' ){
					$prefix = 'width';
				}
			
				$val = isset( $this->value[$name.'-'.$pos] ) ? $this->value[$name.'-'.$pos] : '';
				if( $name == 'border' )
					$val = isset( $this->value[$name.'-width-'.$pos] ) ? $this->value[$name.'-width-'.$pos] : '';
				$output .= '<input type="text" name="' . $name . ( '' !== $prefix ? '_' . $prefix : '' ) . '_' . $pos  . '" data-name="' . $name . ( '' !== $prefix ? '-' . $prefix : '' ) . '-' . $pos . '" class="vc_' . $pos . '" placeholder="-" data-attribute="' . $name . '" value="'.$val.'">';
			}

			return apply_filters( 'vc_css_editor_layer_controls', $output );
		}
	}
}

/**
 * @param $settings
 * @param $value
 *
 * @return mixed|void
 */
function vc_css_editor_form_field( $settings, $value ) {
	$css_editor = new WPBakeryVisualComposerCssEditor();
	$css_editor->settings( $settings );
	$css_editor->value( $value );
	return $output . $css_editor->render();

}
