WebP Express 0.19.0. Conversion triggered using bulk conversion, 2021-01-20 11:01:42

*WebP Convert 2.3.2*  ignited.
- PHP version: 7.4.1
- Server software: Apache/2.4.43 (Win32)

Stack converter ignited

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: C:\Users\Matth\Desktop\Development\BelleVie-shop\app\public/wp-content/uploads/2017/11/home3_1-compressed-150x150.jpg
- destination: C:\Users\Matth\Desktop\Development\BelleVie-shop\app\public/wp-content/webp-express/webp-images/uploads/2017\11\home3_1-compressed-150x150.jpg.webp
- log-call-arguments: true
- converters: (array of 10 items)

The following options have not been explicitly set, so using the following defaults:
- converter-options: (empty array)
- shuffle: false
- preferred-converters: (empty array)
- extra-converters: (empty array)

The following options were supplied and are passed on to the converters in the stack:
- default-quality: 70
- encoding: "auto"
- max-quality: 80
- metadata: "none"
- near-lossless: 60
- quality: "auto"
------------


*Trying: cwebp* 

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: C:\Users\Matth\Desktop\Development\BelleVie-shop\app\public/wp-content/uploads/2017/11/home3_1-compressed-150x150.jpg
- destination: C:\Users\Matth\Desktop\Development\BelleVie-shop\app\public/wp-content/webp-express/webp-images/uploads/2017\11\home3_1-compressed-150x150.jpg.webp
- default-quality: 70
- encoding: "auto"
- low-memory: true
- log-call-arguments: true
- max-quality: 80
- metadata: "none"
- method: 6
- near-lossless: 60
- quality: "auto"
- use-nice: true
- command-line-options: ""
- try-common-system-paths: true
- try-supplied-binary-for-os: true

The following options have not been explicitly set, so using the following defaults:
- alpha-quality: 85
- auto-filter: false
- preset: "none"
- size-in-percentage: null (not set)
- skip: false
- rel-path-to-precompiled-binaries: *****
- try-cwebp: true
- try-discovering-cwebp: true
------------

Encoding is set to auto - converting to both lossless and lossy and selecting the smallest file

Converting to lossy
Looking for cwebp binaries.
Discovering if a plain cwebp call works (to skip this step, disable the "try-cwebp" option)
- Executing: cwebp -version 2>&1. Result: *Exec failed* (return code: 1)

*Output:* 
'cwebp' n'est pas reconnu en tant que commande interne
ou externe, un programme ex�cutable ou un fichier de commandes.

Nope a plain cwebp call does not work
Discovering binaries using "which -a cwebp" command. (to skip this step, disable the "try-discovering-cwebp" option)
Found 0 binaries
Discovering binaries by peeking in common system paths (to skip this step, disable the "try-common-system-paths" option)
Found 0 binaries
Discovering binaries which are distributed with the webp-convert library (to skip this step, disable the "try-supplied-binary-for-os" option)
Checking if we have a supplied precompiled binary for your OS (WINNT)... We do.
Found 1 binaries: 
- C:\Users\Matth\Desktop\Development\BelleVie-shop\app\public\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe
Detecting versions of the cwebp binaries found
- Executing: C:\Users\Matth\Desktop\Development\BelleVie-shop\app\public\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe -version 2>&1. Result: version: *1.1.0*
Binaries ordered by version number.
- C:\Users\Matth\Desktop\Development\BelleVie-shop\app\public\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe: (version: 1.1.0)
Trying the first of these. If that should fail (it should not), the next will be tried and so on.
Creating command line options for version: 1.1.0
Quality of source is 82. This is higher than max-quality, so using max-quality instead (80)
The near-lossless option ignored for lossy
Trying to convert by executing the following command:
C:\Users\Matth\Desktop\Development\BelleVie-shop\app\public\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe -metadata none -q 80 -alpha_q "85" -m 6 -low_memory "C:\Users\Matth\Desktop\Development\BelleVie-shop\app\public/wp-content/uploads/2017/11/home3_1-compressed-150x150.jpg" -o "C:\Users\Matth\Desktop\Development\BelleVie-shop\app\public/wp-content/webp-express/webp-images/uploads/2017\11\home3_1-compressed-150x150.jpg.webp.lossy.webp" 2>&1 2>&1

*Output:* 
Saving file 'C:\Users\Matth\Desktop\Development\BelleVie-shop\app\public/wp-content/webp-express/webp-images/uploads/2017\11\home3_1-compressed-150x150.jpg.webp.lossy.webp'
File:      C:\Users\Matth\Desktop\Development\BelleVie-shop\app\public/wp-content/uploads/2017/11/home3_1-compressed-150x150.jpg
Dimension: 150 x 150
Output:    1168 bytes Y-U-V-All-PSNR 44.46 47.48 46.54   45.15 dB
           (0.42 bpp)
block count:  intra4:         52  (52.00%)
              intra16:        48  (48.00%)
              skipped:        30  (30.00%)
bytes used:  header:             22  (1.9%)
             mode-partition:    254  (21.7%)
 Residuals bytes  |segment 1|segment 2|segment 3|segment 4|  total
  intra4-coeffs:  |     496 |      28 |      51 |      17 |     592  (50.7%)
 intra16-coeffs:  |      33 |       0 |       8 |       3 |      44  (3.8%)
  chroma coeffs:  |     163 |      17 |      33 |      13 |     226  (19.3%)
    macroblocks:  |      48%|       5%|      14%|      33%|     100
      quantizer:  |      27 |      19 |      15 |      11 |
   filter level:  |       8 |       3 |       2 |       0 |
------------------+---------+---------+---------+---------+-----------------
 segments total:  |     692 |      45 |      92 |      33 |     862  (73.8%)

Success
Reduction: 52% (went from 2412 bytes to 1168 bytes)

Converting to lossless
Looking for cwebp binaries.
Discovering if a plain cwebp call works (to skip this step, disable the "try-cwebp" option)
- Executing: cwebp -version 2>&1. Result: *Exec failed* (return code: 1)

*Output:* 
'cwebp' n'est pas reconnu en tant que commande interne
ou externe, un programme ex�cutable ou un fichier de commandes.

Nope a plain cwebp call does not work
Discovering binaries using "which -a cwebp" command. (to skip this step, disable the "try-discovering-cwebp" option)
Found 0 binaries
Discovering binaries by peeking in common system paths (to skip this step, disable the "try-common-system-paths" option)
Found 0 binaries
Discovering binaries which are distributed with the webp-convert library (to skip this step, disable the "try-supplied-binary-for-os" option)
Checking if we have a supplied precompiled binary for your OS (WINNT)... We do.
Found 1 binaries: 
- C:\Users\Matth\Desktop\Development\BelleVie-shop\app\public\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe
Detecting versions of the cwebp binaries found
- Executing: C:\Users\Matth\Desktop\Development\BelleVie-shop\app\public\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe -version 2>&1. Result: version: *1.1.0*
Binaries ordered by version number.
- C:\Users\Matth\Desktop\Development\BelleVie-shop\app\public\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe: (version: 1.1.0)
Trying the first of these. If that should fail (it should not), the next will be tried and so on.
Creating command line options for version: 1.1.0
Trying to convert by executing the following command:
C:\Users\Matth\Desktop\Development\BelleVie-shop\app\public\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe -metadata none -q 80 -alpha_q "85" -near_lossless 60 -m 6 -low_memory "C:\Users\Matth\Desktop\Development\BelleVie-shop\app\public/wp-content/uploads/2017/11/home3_1-compressed-150x150.jpg" -o "C:\Users\Matth\Desktop\Development\BelleVie-shop\app\public/wp-content/webp-express/webp-images/uploads/2017\11\home3_1-compressed-150x150.jpg.webp.lossless.webp" 2>&1 2>&1

*Output:* 
Saving file 'C:\Users\Matth\Desktop\Development\BelleVie-shop\app\public/wp-content/webp-express/webp-images/uploads/2017\11\home3_1-compressed-150x150.jpg.webp.lossless.webp'
File:      C:\Users\Matth\Desktop\Development\BelleVie-shop\app\public/wp-content/uploads/2017/11/home3_1-compressed-150x150.jpg
Dimension: 150 x 150
Output:    8856 bytes (3.15 bpp)
Lossless-ARGB compressed size: 8856 bytes
  * Header size: 1129 bytes, image data size: 7701
  * Lossless features used: PREDICTION CROSS-COLOR-TRANSFORM SUBTRACT-GREEN
  * Precision Bits: histogram=2 transform=2 cache=8

Success
Reduction: -267% (went from 2412 bytes to 8856 bytes)

Picking lossy
cwebp succeeded :)

Converted image in 711 ms, reducing file size with 52% (went from 2412 bytes to 1168 bytes)
