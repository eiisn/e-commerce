WebP Express 0.19.0. Conversion triggered using bulk conversion, 2021-01-20 08:21:52

*WebP Convert 2.3.2*  ignited.
- PHP version: 7.4.1
- Server software: Apache/2.4.43 (Win32)

Stack converter ignited

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: C:\Users\Matth\Desktop\Development\BelleVie-shop\app\public/wp-content/uploads/woocommerce-placeholder-600x600.png
- destination: C:\Users\Matth\Desktop\Development\BelleVie-shop\app\public/wp-content/webp-express/webp-images/uploads/woocommerce-placeholder-600x600.png.webp
- log-call-arguments: true
- converters: (array of 10 items)

The following options have not been explicitly set, so using the following defaults:
- converter-options: (empty array)
- shuffle: false
- preferred-converters: (empty array)
- extra-converters: (empty array)

The following options were supplied and are passed on to the converters in the stack:
- alpha-quality: 80
- encoding: "auto"
- metadata: "none"
- near-lossless: 60
- quality: 85
------------


*Trying: cwebp* 

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: C:\Users\Matth\Desktop\Development\BelleVie-shop\app\public/wp-content/uploads/woocommerce-placeholder-600x600.png
- destination: C:\Users\Matth\Desktop\Development\BelleVie-shop\app\public/wp-content/webp-express/webp-images/uploads/woocommerce-placeholder-600x600.png.webp
- alpha-quality: 80
- encoding: "auto"
- low-memory: true
- log-call-arguments: true
- metadata: "none"
- method: 6
- near-lossless: 60
- quality: 85
- use-nice: true
- command-line-options: ""
- try-common-system-paths: true
- try-supplied-binary-for-os: true

The following options have not been explicitly set, so using the following defaults:
- auto-filter: false
- default-quality: 85
- max-quality: 85
- preset: "none"
- size-in-percentage: null (not set)
- skip: false
- rel-path-to-precompiled-binaries: *****
- try-cwebp: true
- try-discovering-cwebp: true
------------

Encoding is set to auto - converting to both lossless and lossy and selecting the smallest file

Converting to lossy
Looking for cwebp binaries.
Discovering if a plain cwebp call works (to skip this step, disable the "try-cwebp" option)
- Executing: cwebp -version 2>&1. Result: *Exec failed* (return code: 1)

*Output:* 
'cwebp' n'est pas reconnu en tant que commande interne
ou externe, un programme ex�cutable ou un fichier de commandes.

Nope a plain cwebp call does not work
Discovering binaries using "which -a cwebp" command. (to skip this step, disable the "try-discovering-cwebp" option)
Found 0 binaries
Discovering binaries by peeking in common system paths (to skip this step, disable the "try-common-system-paths" option)
Found 0 binaries
Discovering binaries which are distributed with the webp-convert library (to skip this step, disable the "try-supplied-binary-for-os" option)
Checking if we have a supplied precompiled binary for your OS (WINNT)... We do.
Found 1 binaries: 
- C:\Users\Matth\Desktop\Development\BelleVie-shop\app\public\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe
Detecting versions of the cwebp binaries found
- Executing: C:\Users\Matth\Desktop\Development\BelleVie-shop\app\public\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe -version 2>&1. Result: version: *1.1.0*
Binaries ordered by version number.
- C:\Users\Matth\Desktop\Development\BelleVie-shop\app\public\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe: (version: 1.1.0)
Trying the first of these. If that should fail (it should not), the next will be tried and so on.
Creating command line options for version: 1.1.0
Quality: 85. 
The near-lossless option ignored for lossy
Trying to convert by executing the following command:
C:\Users\Matth\Desktop\Development\BelleVie-shop\app\public\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe -metadata none -q 85 -alpha_q "80" -m 6 -low_memory "C:\Users\Matth\Desktop\Development\BelleVie-shop\app\public/wp-content/uploads/woocommerce-placeholder-600x600.png" -o "C:\Users\Matth\Desktop\Development\BelleVie-shop\app\public/wp-content/webp-express/webp-images/uploads/woocommerce-placeholder-600x600.png.webp.lossy.webp" 2>&1 2>&1

*Output:* 
Saving file 'C:\Users\Matth\Desktop\Development\BelleVie-shop\app\public/wp-content/webp-express/webp-images/uploads/woocommerce-placeholder-600x600.png.webp.lossy.webp'
File:      C:\Users\Matth\Desktop\Development\BelleVie-shop\app\public/wp-content/uploads/woocommerce-placeholder-600x600.png
Dimension: 600 x 600
Output:    3128 bytes Y-U-V-All-PSNR 54.01 60.26 62.42   55.38 dB
           (0.07 bpp)
block count:  intra4:        132  (9.14%)
              intra16:      1312  (90.86%)
              skipped:      1283  (88.85%)
bytes used:  header:             61  (2.0%)
             mode-partition:   1151  (36.8%)
 Residuals bytes  |segment 1|segment 2|segment 3|segment 4|  total
  intra4-coeffs:  |    1488 |       2 |      50 |      56 |    1596  (51.0%)
 intra16-coeffs:  |     128 |       0 |      35 |      35 |     198  (6.3%)
  chroma coeffs:  |      86 |       0 |       1 |       7 |      94  (3.0%)
    macroblocks:  |      13%|       0%|       1%|      86%|    1444
      quantizer:  |      20 |      20 |      14 |      12 |
   filter level:  |      13 |       5 |      20 |       2 |
------------------+---------+---------+---------+---------+-----------------
 segments total:  |    1702 |       2 |      86 |      98 |    1888  (60.4%)

Success
Reduction: 90% (went from 31 kb to 3 kb)

Converting to lossless
Looking for cwebp binaries.
Discovering if a plain cwebp call works (to skip this step, disable the "try-cwebp" option)
- Executing: cwebp -version 2>&1. Result: *Exec failed* (return code: 1)

*Output:* 
'cwebp' n'est pas reconnu en tant que commande interne
ou externe, un programme ex�cutable ou un fichier de commandes.

Nope a plain cwebp call does not work
Discovering binaries using "which -a cwebp" command. (to skip this step, disable the "try-discovering-cwebp" option)
Found 0 binaries
Discovering binaries by peeking in common system paths (to skip this step, disable the "try-common-system-paths" option)
Found 0 binaries
Discovering binaries which are distributed with the webp-convert library (to skip this step, disable the "try-supplied-binary-for-os" option)
Checking if we have a supplied precompiled binary for your OS (WINNT)... We do.
Found 1 binaries: 
- C:\Users\Matth\Desktop\Development\BelleVie-shop\app\public\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe
Detecting versions of the cwebp binaries found
- Executing: C:\Users\Matth\Desktop\Development\BelleVie-shop\app\public\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe -version 2>&1. Result: version: *1.1.0*
Binaries ordered by version number.
- C:\Users\Matth\Desktop\Development\BelleVie-shop\app\public\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe: (version: 1.1.0)
Trying the first of these. If that should fail (it should not), the next will be tried and so on.
Creating command line options for version: 1.1.0
Trying to convert by executing the following command:
C:\Users\Matth\Desktop\Development\BelleVie-shop\app\public\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe -metadata none -q 85 -alpha_q "80" -near_lossless 60 -m 6 -low_memory "C:\Users\Matth\Desktop\Development\BelleVie-shop\app\public/wp-content/uploads/woocommerce-placeholder-600x600.png" -o "C:\Users\Matth\Desktop\Development\BelleVie-shop\app\public/wp-content/webp-express/webp-images/uploads/woocommerce-placeholder-600x600.png.webp.lossless.webp" 2>&1 2>&1

*Output:* 
Saving file 'C:\Users\Matth\Desktop\Development\BelleVie-shop\app\public/wp-content/webp-express/webp-images/uploads/woocommerce-placeholder-600x600.png.webp.lossless.webp'
File:      C:\Users\Matth\Desktop\Development\BelleVie-shop\app\public/wp-content/uploads/woocommerce-placeholder-600x600.png
Dimension: 600 x 600
Output:    16046 bytes (0.36 bpp)
Lossless-ARGB compressed size: 16046 bytes
  * Header size: 603 bytes, image data size: 15417
  * Lossless features used: PREDICTION CROSS-COLOR-TRANSFORM SUBTRACT-GREEN
  * Precision Bits: histogram=4 transform=4 cache=9

Success
Reduction: 50% (went from 31 kb to 16 kb)

Picking lossy
cwebp succeeded :)

Converted image in 922 ms, reducing file size with 90% (went from 31 kb to 3 kb)
